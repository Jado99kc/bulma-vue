import Vue from 'vue'
import Vuex from 'vuex'
import router from "../router";

const axios = require('axios').default

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        posts: [],
        post: {
            id: '',
            title: '',
            body: ''
        },
        user: null

    },
    mutations: {

        set_user(state, payload) {
            state.user = payload;
        },
        load_posts(state, payload) {
            state.posts = payload
        },
        add_post(state, payload) {
            state.posts.push(payload)
        },
        edit_post(state, payload) {
            if (!state.posts.find(x => x.id === payload)) {
                router.push('/posts')
            }
            state.post = state.posts.find(x => x.id === payload)
        },
        update_post(state, payload) {
            state.posts = state.posts.map(x => x.id === payload.id ? payload : x)
            router.push('/posts')
        },
        delete_post(state, payload) {
            state.posts = state.posts.filter(x => x.id !== payload);
        }
    },
    actions: {
        verifyLogin({commit}){
            if(localStorage.getItem('user')){
                commit('set_user', JSON.parse(localStorage.getItem('user')))
            }
            else{
                return commit('set_user', null)
            }
        },
        async registerUser({commit}, user) {
            try {
                const newUser = {
                    email: user.email,
                    password: user.pass1,
                    returnSecureToken: true
                }
                const res = await axios.post(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA9e5lwowe-WmfbJUmQNTSdVGZSlhOLOt4`, newUser)
                const userDB = await res.data
                console.log(userDB)
                commit('set_user', userDB)
                await router.push('/posts')
                localStorage.setItem('user', JSON.stringify(userDB))
            } catch (e) {
                console.log(e.response.data.error.message)
            }
        },
        async signIn({commit}, user) {
            try {
                const res = await fetch('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA9e5lwowe-WmfbJUmQNTSdVGZSlhOLOt4', {
                    method: 'POST',
                    body: JSON.stringify({
                        email: user.email,
                        password: user.pass1,
                        returnSecureToken: true
                    })
                })
                const userDB = await res.json()
                console.log(userDB)
                if(userDB.error){
                    return console.log(userDB.error)
                }
                commit('set_user', userDB)
                await router.push('/posts')
                localStorage.setItem('user', JSON.stringify(userDB))
            } catch (e) {
                console.log(e)
            }
        },
        logOut({commit}){
          commit('set_user', null)
            router.push('/')
            localStorage.removeItem('user')
        },
        async loadPosts({commit, state}) {
            try {
                const res = await axios.get(`https://posts-api-bcc54-default-rtdb.firebaseio.com/posts/${state.user.localId}.json?auth=${state.user.idToken}`)
                const dbRes = await res.data;
                const arrayPosts = []
                for (let i in dbRes) {
                    arrayPosts.push(dbRes[i])
                }
                console.log(arrayPosts)
                 commit('load_posts', arrayPosts)
            } catch (e) {
                return console.log(e)
            }
        },
        async getFeed({commit}){
            try {
                const res = await fetch(`https://posts-api-bcc54-default-rtdb.firebaseio.com/posts.json`)
                const dbRes =  await res.json()
                const arrayPosts = []
                for(const i in dbRes) {
                    for (const j in dbRes[i]) {
                        arrayPosts.push(dbRes[i][j])
                    }
                }
                commit('load_posts', arrayPosts)
            }catch (e) {
                console.log(e)
            }
        },
        async addPost({commit,state}, newPost) {
            console.log(newPost.id)
            try {
                const res = await axios.put(`https://posts-api-bcc54-default-rtdb.firebaseio.com/posts/${state.user.localId}/${newPost.id}.json?auth=${state.user.idToken}`, newPost)
                console.log(res)
            } catch (e) {
                console.log(e)
            }
            commit('add_post', newPost)
        },
        editPost({commit}, postId) {
            commit('edit_post', postId)
        },
        async updatePost({commit, state}, post) {
            try {
                const res = await axios.patch(`https://posts-api-bcc54-default-rtdb.firebaseio.com/posts/${state.user.localId}/${post.id}.json?auth=${state.user.idToken}`, post)
                const dataDb = await res.data
                console.log(dataDb)
                commit('update_post', dataDb)
            } catch (e) {
                console.log(e)
            }

        },
        async deletePost({commit, state}, postId) {
            try {
                const res = await axios.delete(`https://posts-api-bcc54-default-rtdb.firebaseio.com/posts/${state.user.localId}/${postId}.json?auth=${state.user.idToken}`)
                const dataDB = await res.data
                console.log(dataDB)
                commit("delete_post", postId)
            } catch (e) {
                console.log(e)
            }
        }
    },
    getters:{
      userIsAuthenticated(state){
          return !!state.user
      }
    },
    modules: {}
})
